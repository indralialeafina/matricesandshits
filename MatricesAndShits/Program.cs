﻿using System;

namespace MatricesAndShits
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //int[,] matrice = Matrice(40, 15);
            //MettreZerosPartout(matrice);
           // AlternerPlusUnMoinsUnSebas(matrice);
            //AfficherMatrice(matrice);
           // IdentiterLaMatrice(matrice);
            //AlternerPlusUnMoinsUn(matrice);
            //AfficherMatrice(matrice);
            TheMatrix matrice = new TheMatrix();
            
        }

        public static int[,] Matrice(int m, int n)
        {
            int[,] mat = new int[m, n];
            return mat;
        }

        public static void AfficherMatrice(int[,] matrice)
        {
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                Console.Write("[");
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    Console.Write($"{matrice[i, j],2}");
                    if (j < matrice.GetLength(1) -1)
                    {
                        Console.Write(", ");
                    }
                }
                Console.WriteLine(" ]");
            }
        }
        public static void AlternerPlusUnMoinsUn(int[,] matrice)
        {
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    if ((i+1 + j+1) % 2 == 0)
                    {
                        matrice[i, j] = 1;
                    }
                    else
                    {
                        matrice[i, j] = -1;
                    }
                }
            }
        }
        public static void IdentiterLaMatrice(int[,] matrice)
        {
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        matrice[i, j] = 1;
                    }
                    else
                    {
                        matrice[i, j] =0 ;
                    }
                }
            }
        }
        
        public static void MettreZerosPartout(int[,] matrice)
        {
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    matrice[i, j] = 0;
                }
            }
        }
    }
}